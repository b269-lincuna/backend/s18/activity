/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/

// ADDITION
	function fSum(fSNum1,fSNum2){
		console.log("Displayed sum of " + fSNum1 + " and " + fSNum2);
		console.log(fSNum1 + fSNum2);
	}
	fSum(5,15);

// SUBTRACTION
	function fDiff(fDNum1,fDNum2){
		console.log("Displayed difference of " + fDNum1 + " and " + fDNum2);
		console.log(fDNum1 - fDNum2);
	}
	fDiff(20,5);

/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/

//MULTIPLICATION
	function fProduct(fPNum1,fPNum2){
		return fPNum1 * fPNum2;
	}
	let product=fProduct(50,10);

	console.log("The product of 50 and 10:");
	console.log(product);

//DIVISION
	function fQuotient(fQNum1,fQNum2){
		return fQNum1 / fQNum2;
	}
	let quotient=fQuotient(50,10);

	console.log("The quotient of 50 and 10:");
	console.log(quotient);

/*
	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/

// AREA OF CIRCLE
// FORMULA A=πr2
// π = 3.14159
// r2 = raduis to the 2nd power

function getCircleArea(radius){
	let intResult=(3.14159 * (radius * radius));
	return intResult.toFixed(2); // added toFixed function to round off in 2 decimal
}

let intRadius = 15;
let circleArea=getCircleArea(intRadius);

console.log("The result of getting the area of circle with " + intRadius + " raduis:");
console.log(circleArea);

/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/

function getAvg(gANum1,gANum2,gANum3,gAnum4){
	return	(gANum1 + gANum2 + gANum3 + gAnum4)/4;
}

let averageVar=getAvg(20,40,60,80);
console.log("The average of 20, 40, 60 and 80:");
console.log(averageVar);


/*

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

function checkIfPassed(score,tScore){
	let scorePercentage = score/tScore;
	let isPassed = (scorePercentage>0.75);
	return isPassed;
}

let myScore=38;
let totalScore=50;

let isPassingScore=checkIfPassed(myScore,totalScore);
console.log("Is " + myScore + "/" + totalScore + " a passing score?");
console.log(isPassingScore);